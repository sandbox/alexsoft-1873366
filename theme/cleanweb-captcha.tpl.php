<div class="cleanweb-captcha">
<?php print render($element['hint']); ?>
<table>
<tbody>
  <tr>
    <td><?php print render($element['image']); ?></td>
    <td><?php print render($element['reload']); ?><?php print render($element['reload_link']); ?></td>
  </tr>
  <tr>
    <td><?php print render($element['response']); ?></td>
    <td><?php print render($element['submit']); ?></td>
  </tr>
  </tbody>
</table>
</div>
