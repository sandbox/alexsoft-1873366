Module cleanweb integrates service Yandex.Clean Web from your site.

Service Yandex.Clean Web is designed to deal with spam in text messages, placed users and automated robots on the pages of web-sites, forums, etc.

Service allows you to detect spam messages and to generate a graphical CAPTCHA to reject requests from the robots.

For spam detection technologies are used service Yandex.SpamSafeGuard, through which the protection of e-mail spam users Yandex.Mail.

Using the API Yandex.Clean Web better to use normal CAPTCHA. In spite of the fact that the service provides the technology CAPTCHA, in a significant number of cases the user free from the necessity of its solving.

Methods of application API

* Forums

If you are the owner of a discussion forum, with the help of the service You will be able to check the message and refuse the publication in that case, if the message is spam.

* Blogs

If You have a popular blog, then check the messages readers through the API Yandex.Clean Web will get rid of "garbage" in the comments.

* Testimonials and reviews

If You are the owner of Internet-shop or e-Mart, the problem of spam in the comments to the goods is for You especially acute. Net Web will allow it to cope with unfair competition.
