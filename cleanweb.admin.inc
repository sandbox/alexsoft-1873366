<?php

/**
 * @file
 * Administrative page callbacks for the cleanweb module.
 */

/**
 * Menu callback: Return cleanweb settings form.
 */
function cleanweb_settings_form() {
  $form = array();

  $form['cleanweb_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Cleanweb API key'),
    '#default_value' => variable_get('cleanweb_apikey', ''),
    // '#description' => t("The maximum number of links to display in the block."),
    '#required' => TRUE,
  );

  $form['cleanweb_save_suspect_submissions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cleanweb save suspect submissions'),
    '#default_value' => variable_get('cleanweb_save_suspect_submissions', 0),
    '#description' => t("Save suspect submissions in watchdog table."),
  );

  // Comment settings.
  $form['cleanweb_comment_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comment form settings'),
  );
  $form['cleanweb_comment_settings']['cleanweb_comment'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable for comment forms'),
    '#default_value' => variable_get('cleanweb_comment', 0),
    '#description' => t('Automatically activate Cleanweb spam check for comment forms.'),
  );

  return system_settings_form($form);
}

/**
 * Validate cleanweb_settings_form submissions.
 */
function cleanweb_settings_form_validate($form, &$form_state) {
  if (strlen($form_state['values']['cleanweb_apikey']) == 0) {
    form_set_error('cleanweb_apikey', t('You must specify cleanweb api key before continue.'));
  }
}