<?php

/**
 * @file
 * Core file for CleanWeb module.
 */

class CleanWeb {

  const CHECK_SPAM_URL = 'http://cleanweb-api.yandex.ru/1.0/check-spam';
  
  const GET_CAPTCHA_URL = 'http://cleanweb-api.yandex.ru/1.0/get-captcha';
  
  const CHECK_CAPTCHA_URL = 'http://cleanweb-api.yandex.ru/1.0/check-captcha';
  
  public $debug = FALSE;
  
  public $requestId = NULL;
  
  public $error = NULL;
  
  private function apiKey() {
    return variable_get('cleanweb_apikey', '');
  }

  protected function xmlQuery($url, $parameters = array(), $method = 'POST') { 
    $this->error = NULL;

    if (!isset($parameters['key'])) {
      $parameters['key'] = self::apiKey(); 
    }

    if ($method == 'GET') {
      $url = url($url, array('query' => $parameters));
      $response = drupal_http_request($url);
    }
    else {
      $options = array(
        'method' => $method,
        'data' => http_build_query($parameters),
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      );
      $response = drupal_http_request($url, $options);
    }

    // Need more tests here, no clear information about response standarts at http://api.drupal.org/api/drupal/includes!common.inc/function/drupal_http_request/7
    if ($response->code != 200) {
      //$this->error = (string) new SimpleXMLElement($response->data)->message;
      return FALSE;
    }

    return new SimpleXMLElement($response->data); 
  }
  
  public function isSpam($params = array()) {
    $xml = $this->xmlQuery(self::CHECK_SPAM_URL, $params, 'POST');
    $this->requestId = (string) $xml->id;
    return (string) $xml->text['spam-flag'] === 'yes' || $this->debug;
  }
  
  public function getCaptcha() {
    $params = array();
    if (is_null($this->requestId)) {
      // exception
    }
    $params['id'] = $this->requestId;
    $xml = $this->xmlQuery(self::GET_CAPTCHA_URL, $params, 'GET');

    $captcha = new stdClass();
    $captcha->requestId = $this->requestId;
    $captcha->captcha = (string) $xml->captcha;
    $captcha->url = (string) $xml->url;

    return $captcha;
  }
  
  public function checkCaptcha(stdClass $captcha, $value) {
    $params = array(
      'id' => $captcha->requestId,
      'captcha' => $captcha->captcha,
      'value' => $value
    );
    $xml = $this->xmlQuery(self::CHECK_CAPTCHA_URL, $params, 'GET');
  }

}